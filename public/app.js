const SSO = 'http://localhost:3030'; 
const returnTo = `${SSO}?return_to=${encodeURIComponent('http://localhost')}`; 
const ssoFrameSrc = `${SSO}/sso_inject.html`

const feathersSSOClient = feathers().configure(feathers.rest(SSO).fetch(fetch));
feathersSSOClient.configure(feathers.hooks());
feathersSSOClient.configure(feathers.authentication({
    storage: window.localStorage
}));

const users = feathersSSOClient.service('users');

$(document).ready(() => {
  $(window).on("message", (ev) => {
    const token = ev.originalEvent.data;
    if(feathersSSOClient.passport.payloadIsValid(token)) {
      feathersSSOClient.logout();
      feathersSSOClient.authenticate({
        strategy: 'jwt', 
        accessToken: token
      }).then((token) => {
        console.log('User is logged in', token);
        loadUsers();
      }).catch((err) => {
        showInfo(err, 'red')
      });
    } else {
      showInfo('Bad JWT', 'red');
    }
  }); 

  const $sso_inject = $(`<iframe  src="${ssoFrameSrc}"></iframe>`).hide();
  $('body').append($sso_inject);
    
}); 

$( "#redirect_").click(( ) =>  {
    window.location.replace(returnTo);
});

function loadUsers() {
  users.find().then((page) => {
    const userDescr = page.data[0];
    showInfo(`Nice to meet you, ${userDescr.firstName} ${userDescr.lastName}`, 'green');
  }).catch((err) => {
    showInfo(err, 'red');
  });
};

function showInfo(txt, color) {
  $('#info').text(txt).css('color', color);
}
//debug DRY
function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
